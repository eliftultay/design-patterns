package decorator.beverages;

public abstract class Beverage {

    public String description =  "undefined description";
    public Size size = Size.SMALL;

    public String getDescription() {
        return description;
    }

    public Size setSize(Size size) {
        this.size = size;
        return size;
    }

    public Size getSize() {
        return size;
    }

    public abstract double cost();

}
