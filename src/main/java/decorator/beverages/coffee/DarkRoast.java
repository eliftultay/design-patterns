package decorator.beverages.coffee;

import decorator.beverages.Beverage;
import decorator.beverages.Size;

import java.text.DecimalFormat;

public class DarkRoast extends Beverage {

    public DarkRoast() {
        description = "Dark Roast Coffee";
    }

    @Override
    public double cost() {
        double cost = .99;
        if(getSize() == Size.SMALL) {
            cost += .10;
        } else if(getSize() == Size.MEDIUM) {
            cost +=  .15;
        } else if(getSize() == Size.LARGE) {
            cost += .18;
        }
        return Double.parseDouble(new DecimalFormat("##.##").format(cost));
    }
}
