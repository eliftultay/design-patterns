package decorator.beverages.coffee;

import decorator.beverages.Beverage;
import decorator.beverages.Size;

import java.text.DecimalFormat;

public class HouseBlend extends Beverage {
    public HouseBlend() {
        description = "House Blend Coffee";
    }

    @Override
    public double cost() {
        double cost = 0.89;
        if(getSize() == Size.SMALL) {
            cost += .10;
        } else if(getSize() == Size.MEDIUM) {
            cost += .13;
        } else if(getSize() == Size.LARGE) {
            cost += .15;
        }
        return Double.parseDouble(new DecimalFormat("##.##").format(cost));
    }
}
