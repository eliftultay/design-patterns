package decorator.beverages.coffee;

import decorator.beverages.Beverage;
import decorator.beverages.Size;

import java.text.DecimalFormat;

public class Decaf extends Beverage {

    public Decaf() {
        description = "Decaf Coffee";
    }

    @Override
    public double cost() {
        double cost = 1.05;
        if(getSize() == Size.SMALL) {
            cost += .18;
        } else if(getSize() == Size.MEDIUM) {
            cost +=  .23;
        } else if(getSize() == Size.LARGE) {
            cost += .28;
        }
        return Double.parseDouble(new DecimalFormat("##.##").format(cost));
    }
}
