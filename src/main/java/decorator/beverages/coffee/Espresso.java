package decorator.beverages.coffee;

import decorator.beverages.Beverage;
import decorator.beverages.Size;

import java.text.DecimalFormat;

public class Espresso extends Beverage {
    public Espresso() {
        description = "Espresso Coffee";
    }

    @Override
    public double cost() {
        double cost = 1.99;
        if(getSize() == Size.SMALL) {
            cost += .10;
        } else if(getSize() == Size.MEDIUM) {
            cost += .13;
        } else if(getSize() == Size.LARGE) {
            cost += .17;
        }
        return Double.parseDouble(new DecimalFormat("##.##").format(cost));
    }
}
