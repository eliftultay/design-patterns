package decorator.beverages;

public abstract class CondimentDecorator extends Beverage{

    @Override
    public abstract String getDescription();

}
