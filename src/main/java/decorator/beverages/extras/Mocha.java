package decorator.beverages.extras;

import decorator.beverages.Beverage;
import decorator.beverages.CondimentDecorator;
import decorator.beverages.Size;

import java.text.DecimalFormat;

public class Mocha extends CondimentDecorator {
    Beverage beverage;

    public Mocha(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + ", Mocha";
    }

    @Override
    public Size getSize() {
        return beverage.getSize();
    }

    @Override
    public double cost() {
        double cost = beverage.cost();
        if(getSize() == Size.SMALL) {
            cost += .20;
        } else if(getSize() == Size.MEDIUM) {
            cost +=  .27;
        } else if(getSize() == Size.LARGE) {
            cost += .35;
        }
        return Double.parseDouble(new DecimalFormat("##.##").format(cost));
    }
}
