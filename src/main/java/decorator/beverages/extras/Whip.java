package decorator.beverages.extras;

import decorator.beverages.Beverage;
import decorator.beverages.CondimentDecorator;
import decorator.beverages.Size;

import java.text.DecimalFormat;

public class Whip extends CondimentDecorator {
    Beverage beverage;

    public Whip(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + ", Whip";
    }

    @Override
    public Size getSize() {
        return beverage.getSize();
    }

    @Override
    public double cost() {
        double cost = beverage.cost();
        if(getSize() == Size.SMALL) {
            cost += .10;
        } else if(getSize() == Size.MEDIUM) {
            cost +=  .20;
        } else if(getSize() == Size.LARGE) {
            cost += .30;
        }
        return Double.parseDouble(new DecimalFormat("##.##").format(cost));
    }
}
