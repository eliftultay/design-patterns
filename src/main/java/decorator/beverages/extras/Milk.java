package decorator.beverages.extras;

import decorator.beverages.Beverage;
import decorator.beverages.CondimentDecorator;
import decorator.beverages.Size;

import java.text.DecimalFormat;

public class Milk extends CondimentDecorator {
    Beverage beverage;

    public Milk(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return beverage.getDescription() + ", Milk";
    }

    @Override
    public Size getSize() {
        return beverage.getSize();
    }

    @Override
    public double cost() {
        double cost = beverage.cost();
        if(getSize() == Size.SMALL) {
            cost += .10;
        } else if(getSize() == Size.MEDIUM) {
            cost +=  .15;
        } else if(getSize() == Size.LARGE) {
            cost += .20;
        }
        return Double.parseDouble(new DecimalFormat("##.##").format(cost));
    }
}
