package decorator.beverages;

public enum Size {
    UNDEFINED,
    SMALL,
    MEDIUM,
    LARGE
}
