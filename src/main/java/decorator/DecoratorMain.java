package decorator;

import decorator.beverages.Beverage;
import decorator.beverages.Size;
import decorator.beverages.coffee.DarkRoast;
import decorator.beverages.coffee.Espresso;
import decorator.beverages.coffee.HouseBlend;
import decorator.beverages.extras.Mocha;
import decorator.beverages.extras.Whip;

public class DecoratorMain {
    public static void main(String [] args) {

        Beverage beverage = new Espresso();
        beverage.setSize(Size.MEDIUM);
        System.out.println(beverage.getDescription()
                + " "
                + beverage.getSize()
                + " $"
                + beverage.cost());

        Beverage beverage2 = new DarkRoast();
        beverage2.setSize(Size.SMALL);
        beverage2 = new Mocha(beverage2);
        beverage2 = new Whip(beverage2);
        System.out.println(beverage2.getDescription()
                + " "
                + beverage2.getSize()
                + " $"
                + beverage2.cost());

        Beverage beverage3 = new HouseBlend();
        beverage3.setSize(Size.LARGE);
        beverage3 = new Whip(beverage3);
        System.out.println(beverage3.getDescription()
                + " "
                + beverage2.getSize()
                + " $"
                + beverage3.cost());

    }
}
