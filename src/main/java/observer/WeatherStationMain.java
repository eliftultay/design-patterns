package observer;

import observer.display.CurrentConditionsDisplay;
import observer.display.ForecastDisplay;
import observer.display.StatisticsDisplay;
import observer.weather.WeatherData;

public class WeatherStationMain {
    public static void main(String [] args) {
        WeatherData weatherData = new WeatherData();

        CurrentConditionsDisplay currentConditionsDisplay = new
                CurrentConditionsDisplay(weatherData);

        StatisticsDisplay statisticsDisplay = new StatisticsDisplay(weatherData);

        ForecastDisplay forecastDisplay = new ForecastDisplay(weatherData);

        weatherData.setWeatherMeasurements(80, 65, 30.4f);
        weatherData.setWeatherMeasurements(82, 70, 29.2f);
        weatherData.setWeatherMeasurements(78, 90, 29.2f);
    }
}
