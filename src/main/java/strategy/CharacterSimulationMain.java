package strategy;

import strategy.SimulateStrategyPattern;

import java.util.ArrayList;
import java.util.List;

public class CharacterSimulationMain {
    public static void main(String [] args) {
        System.out.println("Hello Sunshine!");

        SimulateStrategyPattern strategyPattern = new SimulateStrategyPattern();

        strategyPattern.callCharacters();
    }
}
