package strategy;

import strategy.behaviors.WeaponBehaviour;

public abstract class Characters {

    WeaponBehaviour weapon;

    public Characters(){

    }
    public abstract void fight();

    public void performUseWeapon() {
        weapon.useWeapon();
    }

    public void setWeapon(WeaponBehaviour weaponBehaviour){
        weapon = weaponBehaviour;
    }
}
