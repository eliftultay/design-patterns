package strategy.behaviors;

public interface WeaponBehaviour {
    public void useWeapon();
}
