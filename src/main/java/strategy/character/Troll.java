package strategy.character;

import strategy.Characters;
import strategy.behaviors.AxeBehaviour;

public class Troll extends Characters {

    public Troll() {
        setWeapon(new AxeBehaviour());
    }

    @Override
    public void fight() {
        System.out.println("Troll Fight.");
    }
}
