package strategy.character;

import strategy.Characters;
import strategy.behaviors.SwordBehaviour;

public class Queen extends Characters {
    public Queen() {
        setWeapon(new SwordBehaviour());
    }

    @Override
    public void fight() {
        System.out.println("Queen Fight.");
    }
}
