package strategy.character;

import strategy.Characters;
import strategy.behaviors.KnifeBehaviour;

public class King extends Characters {
    public King() {
        setWeapon(new KnifeBehaviour());
    }

    @Override
    public void fight() {
        System.out.println("King Fight.");
    }
}
