package strategy.character;

import strategy.Characters;
import strategy.behaviors.BowAndArrowBehaviour;

public class Knight extends Characters {
    public Knight() {
        setWeapon(new BowAndArrowBehaviour());
    }

    @Override
    public void fight() {
        System.out.println("Knight fight.");
    }
}
