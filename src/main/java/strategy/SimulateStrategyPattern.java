package strategy;

import strategy.character.King;
import strategy.character.Knight;
import strategy.character.Queen;
import strategy.character.Troll;

public class SimulateStrategyPattern {

    Characters king = new King();
    Characters queen = new Queen();
    Characters troll = new Troll();
    Characters knight = new Knight();

    public void callCharacters(){
        king.fight();
        king.performUseWeapon();

        queen.fight();
        queen.performUseWeapon();

        troll.fight();
        troll.performUseWeapon();

        knight.fight();
        knight.performUseWeapon();
    }
}
